God Of War
Es una de las franquicias mas importantes de videojuegos de la famosa empresa Sony, siendo un gran referente de los videojuegos de lucha y accion.

Curiosidades:
-El protagonista de la saga kratos, esta inspirado en el personaje mitologico Griego Kratos, la personoficacion masculina de la fuerza y poder, aunque no se trata del mismo personaje, ya que este ultimo era hijo del titan Palas, el titan de la guerra, y Estigia, la Diosa del odio.

-Kratos mide 1.90 y pesa 110 Kg.

-En la saga de videojuegos, Kratos, fue el responsable de la destruccion del Olimpo y el hundimiento de la Atlantis.

-En toda la saga, Kratos no sonrie ni una sola vez.

-En realidad no se tenia planeado la creacion de mas secuelas despues del primer videojuego, por lo que God of War 1 seria el unico, pero debido a su gran exito, Santa Monica decidio crear mas secuelas.

Mejores jefes de la saga:
10. Tanatos:
Este es el Dios de la muerte, y el carcelero del hermano de Kratos: Deimos. Este jefe tendra su aparicion en god of war: Ghosts of Sparta en el cual sera asesinado por el protagonista.

9. Ares:
Este es el antagonista principal del primer videojuego de la saga, y el primer Dios de la guerra. Este tendra una pelea con el protagonista en la ciudad de Atenas, en la cual sera derrotado, siendole otorgado el titulo de Dios de la guerra a su asesino, Kratos.

8. Zeus:
Este es el Dios del Olimpo, y el padre de Kratos. Gran parte de la saga consiste en la redencion y venganza del protagonista, quien desea eliminar a Zeus por haberle traicionado.
Zeus es eliminado junto con su madre Gaia en el monte Olimpo a manos del Espartano.

7. Hidra de Lerna:
Este es el primer jefe de toda la saga, por lo que fue considerado en su momento como el mejor jefe de la saga. Este tiene su aparicion en el mar Egeo.

6. Las hermanas del destino:
Son la trama principal de God of war 2. Estas son asesinadas por Kratos, quien buscaba su ayuda para cambiar su destino.

5. Heracles:
Este es el hermano de Kratos. Fue derrotado en un combate brutal en la arena del monte Olimpo.

4. Cronos:
El padre de Zeus, destinado a vagar por el tartaro por un castigo de su hijo, es asesinado con una piedra de Lonfalos (cristal de gran resistencia).

3. Coloso de rodas:
Es el primer jefe de God of War 2, y adquiere su poder debido a la traicion de los Dioses, con el objetivo de eliminar a Kratos.

2. El Minotauro:
Esta bestia es eliminada por Kratos en las pruebas de hades, siendo el protector de Pandora, el objetivo principal de la entrega.

1. Hades:
Este tendra su aparicion en la tercera entrega, en sus aposentos situados en el inframundo en donde intentara vengarse por el asesinato de su esposa Persefone.
En su primera fase, este perdera su carne, la cual es arrancada de su cuerpo por las espadas de Atenea. En su segunda fase, ya acabado, este incrementara su tama�o para intentar vencer, siendo victima de sus propias armas, con las que le sera arrancada el alma liberando asi, todas las almas del inframundo.

Galeria:

GOW4 (trailer):

https://www.youtube.com/watch?v=AN3jEjjcZ-k

https://www.youtube.com/watch?v=L1-HReFsZ2o


